<?php include("layouts/master.php"); ?>
<div id="header-contacto">
  <div class="container-fluid nivelesbg valign-wrapper">
    <div class="row center-align">
      <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> CONTACTO </h1>
    </div>
  </div>
</div>
<div class="container" id="form-patria">
    <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Nombre Completo" id="first_name" type="text" class="validate">
          <label for="first_name">Nombre Completo</label>
        </div>
        <div class="input-field col s6">
          <input id="telephone" type="tel" class="validate">
          <label for="telephone">Número Telefónico</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="email" type="email" class="validate">
          <label for="email">Email</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea1" class="materialize-textarea"></textarea>
          <label for="textarea1">Mensaje</label>
        </div>
      </div>
      <div class="row">
        <div class="col l6 m6 s12">
          <input class="btn-patria btn" name="borrar" type="reset" value="Borrar">
          <input class="btn-patria btn" name="enviar" type="submit" value="Enviar">
        </div>
      </div>
    </form>
  </div>
</div>
<div class="container" id="info-contact">
  <div class="row center-align">
    <div class="col l4 m4 s12">
      <i class="material-icons">call</i>
      <h3> TELÉFONOS </h3>
      <p> Plantel Ángel (55) 5797 9206</p>
      <p> Plantel Iturbide (55) 5765 4821</p>
    </div>
    <div class="col l4 m4 s12">
      <i class="material-icons">location_city</i>
      <h3> DIRECCIÓN </h3>
      <p> Avenida Ángel de la Independencia #16 y 18<br>
          Colonia Metropolitana 2a sección CP 5770<br>
          Nezahualcóyotl, Estado de México<br>
    </div>
    <div class="col l4 m4 s12">
      <i class="material-icons">local_post_office</i>
      <h3> CORREO ELECTRÓNICO </h3>
      <p>relacionespublicas@colegiopatria.com.mx </p>
    </div>
  </div>
</div>
<div id="map"></div>
</div>

<?php include("layouts/footer.php"); ?>
