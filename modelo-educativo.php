<?php include("layouts/master.php"); ?>
  <div id="header-modelo">
    <div class="container-fluid modelobg valign-wrapper">
      <div class="row center-align">
        <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> MODELO EDUCATIVO </h1>
      </div>
    </div>
  </div>
  <div id="intro-mod">
    <div class="container">
     <div class="row center-align">
      <div class="col l12 m12 s12">
        <p class= "nos-content wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"> Nuestro Modelo Educativo se diseñó para que los alumnos y sus familias consigan el mayor beneficio posible del proceso enseñanza aprendizaje, así como de su estancia en la institución. El Modelo Educativo Patria está centrado en el alumno y se enfoca en desarrollar en él las competencias necesarias para que se convierta en una persona triunfadora y exitosa, con los conocimientos, hábitos, valores y principios que le permitan superar obstáculos e integrarse en la vida productiva, siendo un factor de cambio positivo y benéfico para su familia, su comunidad y el país.</p>
        <h2 class="wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"> Entre sus características están: </h2>
      </div>
    </div>
  </div>
  <div id="bullets-modelo">
    <div class="container-fluid">
      <div class="row">
        <div class="col l6 m6 s12">
          <img class="responsive-img wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay="1s" src="img/modelo1.jpg">
        </div>
        <div class="col l6 m6 s12 wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="1s">
          <ul>
            <li>Centrado en el alumno y enfocado en el desarrollo de competencias para la vida;</li>
            <li>Permanente actualización de Planes y Programas de Estudio con fundamento en las nuevas tendencias educativas nacionales e internacionales;</li>
            <li>Capacitación permanente del personal docente;</li>
            <li>El material didáctico se actualiza y adquiere de manera constante;</li>
            <li>Evalúa continuamente el cumplimiento de objetivos y logros académicos;</li>
            <li>Alumnos y padres de familia reciben atención personalizada;</li>
            <li>La satisfacción de las necesidades de alumnos, padres de familia y sociedad en general es motivo de acción y búsqueda constante;</li>
            <li>Apoyo psicopedagógico y Escuela para Padres;</li>
            <li>La integridad del alumno es fundamental, por lo que se dispone de servicio médico permanente;</li>
            <li>Realización de Talleres Extracurriculares que forman integralmente a los alumnos.</li>

          </ul>
        </div>
    </div>
  </div>


<?php include("layouts/footer.php"); ?>
