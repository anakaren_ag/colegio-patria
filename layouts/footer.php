<footer class="page-footer footer-patria hide-on-mobile">
  <div class="container">
    <div class="row">
      <div class="col l3">
        <img class="footer-logo responsive-img" src="img/cp-footerlogo.png">
      </div>
      <div class="col l3">
        <h5 class="footer-title hide-on-med-and-down">Menú de navegacion</h5>
        <ul>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Quiénes somos</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Niveles</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Beneficios</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Planteles</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Galería</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Contacto</a></li>
        </ul>
      </div>
      <div class="col l3">
        <h5 class="footer-title hide-on-med-and-down">Niveles</h5>
        <ul>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Maternal</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Pre-escolar</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Primaria</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Secundaria</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Preparatoria</a></li>
        </ul>
      </div>
      <div class="col l3">
        <h5 class="footer-title hide-on-mobile hide-on-med-and-down">Corporativo</h5>
        <ul>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Boletines</a></li>
          <li><a class="grey-text text-lighten-3 hide-on-med-and-down" href="#!">Trabaja con nosotros</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    © 2017 Colegio Patria
    <a class="grey-text text-lighten-4 right" href="#!">Aviso de Privacidad</a>
    </div>
  </div>
</footer>
</main>
<script src="http://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
<script src="https://use.fontawesome.com/365e705044.js"></script>
<script src="js/wow.js"></script>
<script src="js/scripts.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.materialboxed').materialbox();
  });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtmUy57eJB0qWWnv7KwGsOFsgIGJkErB0&callback=initMap"
async defer></script>
<script type= "text/javascript">
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    scrollwheel: false,
    center: {lat: 19.3953929, lng: -99.0435664}
  });

  setMarkers(map);
}

  var locations = [
    ['Plantel Ángel', 19.3893199, -99.0333794, 2],
    ['Maroubra Beach', 19.3953929, -99.0435664, 1]
  ];

  function setMarkers(map) {
    var image = {
      url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
      size: new google.maps.Size(29, 32),
      origin: new google.maps.Point(0,0),
      anchor: new google.maps.Point(0,32)
    };

    var shape = {
      coords: [1,1,1,20,18,20,18,1],
      type: 'poly'
    };

    for (var i = 0;i < locations.length; i++) {
      var location = locations[i];
      var marker = new google.maps.Marker({
        position: {lat: location[1], lng: location[2]},
        map: map,
        icon: image,
        shape: shape,
        title: location[0],
        zIndex: location[3]
      });
    }
  }
</script>
</body>
</html>
