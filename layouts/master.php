<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Colegio Patria nació en el año de 1994 para contribuir a la formación del ser humano con educación de calidad.">
	<meta name="keywords" content="escuela, primaria, secundaria, preparatoria, educacion">
	<meta name="author" content="Appeal Developing ">
	<title>Sociedad Cultural Colegio Patria, S.C</title>
	<!-- STYLES -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Slab:400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/animate.css">
  </head>
  <body>
		<header>
			<nav>
			    <div class="nav-wrapper">
			      <ul id="" class="right hide-on-med-and-down">
							<li><a class="dropdown-button wow slideInDown" href="#!" data-activates="dropdown1" data-wow-duration="2s" data-wow-delay="0.8s">Nosotros<i class="material-icons right">arrow_drop_down</i></a></li>
		          <li><a class="dropdown-button wow slideInDown" href="niveles.php" data-activates="dropdown2" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">Niveles<i class="material-icons right">arrow_drop_down</i></a></li>
		          <li><a href="beneficios.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">¿Por qué elegirnos?</a></li>
		          <li><a href="galeria.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s" >Galería</a></li>
							<li><a href="#" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s"><b>Comunidad Patria</b></a></li>
		          <li><a href="contacto.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">Contáctanos</a></li>
			      </ul>
			    </div>
			  </nav>

			<div class="container">
				<a href="#" data-activates="nav-mobile" class="button-collapse top-nav full hide-on-large-only">
				<i class="material-icons">menu</i></a>
			</div>

			<ul id="nav-mobile" class="side-nav fixed">
				<a href="/">
						<img class="nav-logo wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.5s" src="img/cp-logo.png">
				</a>
				<div class="padding"></div>
				<li class="no-padding">
					<ul class="collapsible collapsible-accordion">
						<li><a class="waves-effect waves-teal" href="#">Eventos Culturales</a></li>
						<li><a class="waves-effect waves-teal" href="#">Actividades Extracurriculares</a></li>
						<li><a class="waves-effect waves-teal" href="#">Nuestros Logros</a></li>
						<li><a class="waves-effect waves-teal" href="#">Boletines</a></li>
						<li><a class="waves-effect waves-teal" href="#">Responsabilidad Social</a></li>
						<li><a class="waves-effect waves-teal" href="#">Aviso de privacidad</a></li>
					</ul>
				</ul>
	</header>
	<main>
    <!-- <div class="container-fluid">
        <div class="row center-align">
					<a href="/">
          	<img class="nav-logo wow fadeInDown" data-wow-duration="2s" data-wow-delay="0.5s" src="img/cp-logo.png">
					</a>
        </div>
    </div>
    <div class="container-fluid">
      <ul id="dropdown1" class="dropdown-content" data-wow-duration="2s" data-wow-delay="0.8s">
				<li><a href="quienes-somos.php">QUIÉNES SOMOS</a></li>
				<li><a href="modelo-educativo.php">MODELO EDUCATIVO</a></li>
        <li><a href="principios.php">PRINCIPIOS Y VALORES </a></li>
      </ul>
			<ul id="dropdown2" class="dropdown-content" data-wow-duration="2s" data-wow-delay="0.8s">
				<li><a href="maternal.php">MATERNAL</a></li>
				<li><a href="preescolar.php">PREESCOLAR</a></li>
				<li><a href="primaria.php">PRIMARIA</a></li>
				<li><a href="secundaria.php">SECUNDARIA</a></li>
				<li><a href="preparatoria.php">PREPARATORIA</a></li>
			</ul>
			<ul id="dropdown3" class="dropdown-content" data-wow-duration="2s" data-wow-delay="0.8s">
        <li><a href="quienes-somos.php">QUIÉNES SOMOS</a></li>
				<li><a href="modelo-educativo.php">MODELO EDUCATIVO</a></li>
        <li><a href="principios.php">PRINCIPIOS Y VALORES </a></li>

      </ul>
			<ul id="dropdown4" class="dropdown-content" data-wow-duration="2s" data-wow-delay="0.8s">
				<li><a href="maternal.php">MATERNAL</a></li>
				<li><a href="preescolar.php">PREESCOLAR</a></li>
				<li><a href="primaria.php">PRIMARIA</a></li>
				<li><a href="secundaria.php">SECUNDARIA</a></li>
				<li><a href="preparatoria.php">PREPARATORIA</a></li>
			</ul>
      <nav>
        <div class="nav-wrapper nav-patria">
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul id="nav-mobile" class="center-menu hide-on-med-and-down">
            <li><a class="dropdown-button wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s" href="#" data-activates="dropdown3">Nosotros<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a class="dropdown-button wow slideInDown" href="niveles.php" data-activates="dropdown4" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">Niveles<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a href="beneficios.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">¿Por qué elegirnos?</a></li>
            <li><a href="galeria.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s" >Galería</a></li>
						<li><a href="#" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s"><b>Comunidad Patria</b></a></li>
            <li><a href="contacto.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">Contáctanos</a></li>
          </ul>
					<ul class="side-nav" id="mobile-demo">
        		<li><a class="dropdown-button" href="#" data-activates="dropdown1">Nosotros<i class="material-icons right">arrow_drop_down</i></a></li>
        		<li><<a class="dropdown-button" href="niveles.php" data-activates="dropdown2">Niveles<i class="material-icons right">arrow_drop_down</i></a></li>
        		<li><a href="beneficios.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">¿Por qué elegirnos?</a></li>
        		<li><a href="galeria.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s" >Galería</a></li>
						<li><a href="#" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s"><b>Comunidad Patria</b></a></li>
            <li><a href="contacto.php" class="wow slideInDown" data-wow-duration="2s" data-wow-delay="0.8s">Contáctanos</a></li>
      		</ul>
        </div>
      </nav>
    </div> -->
