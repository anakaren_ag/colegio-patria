$(".dropdown-button").dropdown();
$(document).ready(function(){
$('.slider').slider();
});
$(".button-collapse").sideNav();
$('.dropdown-button').dropdown({
   inDuration: 300,
   outDuration: 225,
   constrainWidth: false, // Does not change width of dropdown to that of the activator
   hover: true, // Activate on hover
   gutter: 0, // Spacing from edge
   belowOrigin: true, // Displays dropdown below the button
   alignment: 'left', // Displays dropdown with edge aligned to the left of button
   stopPropagation: false // Stops event propagation
 }
);  $(document).ready(function(){
  $('.slider').slider();
  });
  $(".button-collapse").sideNav();
  $('.dropdown-button').dropdown({
     inDuration: 600,
     outDuration: 525,
     constrainWidth: false, // Does not change width of dropdown to that of the activator
     hover: true, // Activate on hover
     gutter: 0, // Spacing from edge
     belowOrigin: true, // Displays dropdown below the button
     alignment: 'left', // Displays dropdown with edge aligned to the left of button
     stopPropagation: false // Stops event propagation
   }
 );

  new WOW().init();

  //Galería
  $(function (){

  // Cache vars
  var $gallery = $('.gallery'),
      $lightbox = $('.lightbox'),
      $figure = $('figure'),
      $close = $('.close');

  // Dribbble API shizzz
  var token = 'b5501bcf1ef8eca0ef89aa3982ca742556d65b1832e077019397bbe0960df317';
  var url = 'https://api.dribbble.com/v1/shots?sort=recent&access_token=' + token;

  // Grab Dribbble popular
  $.getJSON(url, function(data) {

    $.each(data, function(index, shot) {

      var thumb = shot.images.teaser,
          full = shot.images.hidpi || shot.images.normal;

      var item = $('<li class="item" data-full="'+full+'"><img src="'+thumb+'" /></li>');

      $gallery.append(item);

    });

    // Handle item click
    $('.item').on('click', function() {

      var full = $(this).attr('data-full');

      toggleLightbox(full);

      console.log(full);

    });

    // Toggle lightbox
    function toggleLightbox(url) {

      if ($lightbox.is('.open')) {

        $lightbox
          .removeClass('open')
          .fadeOut(200);

      } else {

        $figure.css('background-image', 'url(' + url + ')');
        $lightbox
          .addClass('open')
          .fadeIn(200);

      }

    }

    // Close
    $lightbox.on('click', toggleLightbox);

});

});
