<?php include("layouts/master.php"); ?>
  <div id="header-beneficios">
    <div class="container-fluid beneficiosbg valign-wrapper">
      <div class="row center-align">
        <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> BENEFICIOS </h1>
      </div>
    </div>
  </div>
  <div id="welcome-beneficios">
    <div class="container">
      <div class="row center-align">
        <div class="col l12 m12 s12">
          <p class="niveles-beneficios wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s">
            Son múltiples los beneficios que Colegio Patria ofrece a los usuarios de sus servicios educativos. <br>Desde una perspectiva general podemos mencionar, de manera enunciativa, no limitativa, las siguientes:
          </p>
          <div class="line"></div>
        </div>
      </div>
    </div>
  </div>
  <div id="beneficios1">
    <div class="container-fluid p-top-bottom">
      <div class="row">
        <div class="col l6 m6 s12 maternal-bg">
        </div>
        <div class="col l6 m6 s12">
            <ul class= "wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.8s">
              <li class="circle-patria">Su hijo realizará sus estudios en una institución de vanguardia, innovadora y de calidad,<br> &nbsp;&nbsp;que está debidamente Certificada en ISO 9001:2008<br></li>
              <li class="circle-patria">El alumno desarrollará las competencias para la vida que le serán indispensables para ser exitoso<br></li>
              <li class="circle-patria">El alumno recibirá clases de personal docente permanente capacitado<br></li>
              <li class="circle-patria">Usted tendrá la certeza de que los Planes y Programas de Estudio están actualizados con base en<br>&nbsp;&nbsp; las nuevas tendencias educativas nacionales e internacionales<br></li>
              <li class="circle-patria">Para el mejor desempeño académico, su hijo(a) tendrá acceso a material de apoyo didáctico<br>&nbsp;&nbsp; debidamente actualizado<br></li>
              <li class="circle-patria">Usted y su hijo(a) recibirán atención personalizada<br></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
  <div id="beneficios2">
    <div class="container-fluid p-bottom">
      <div class="row">
        <div class="col l6 m6 s12">
          <ul class= "wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.8s">
            <li class="circle-patria">Dispondrá de acceso al apoyo psicopedagógico que requiera y a Escuela para Padres</li>
            <li class="circle-patria">El alumno contará con Servicio Médico permanente</li>
            <li class="circle-patria">Podrá acceder al Taller Extracurricular que sea de su interés</li>
            <li class="circle-patria">El alumno tomará sus clases en instalaciones limpias, seguras y modernas</li>
            <li class="circle-patria">Usted tendrá la certeza de que su hijo(a) se encuentra, durante la jornada escolar, en un entorno sano, de concordia y, sobre todo, seguro</li>
            <li class="circle-patria">Usted y su familia tendrán a su alcance toda una gama de servicios que le facilitarán y harán más cómodo y eficiente el proceso educativo del alumno</li>
          </ul>
        </div>
        <div class="col l6 m6 s12 maternal-bg">
        </div>
      </div>
    </div>
  </div>
  <div id="beneficios3">
    <div class="container-fluid p-top-bottom">
      <div class="row">
        <div class="col l6 m6 s12 maternal-bg">
        </div>
        <div class="col l6 m6 s12">
          <ul class= "wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0.8s">
           <li class="circle-patria">Tendrá también a su disposición una gran cantidad de opciones para que el alumno se desarrolle integralmente</li>
           <li class="circle-patria">Contará con el profesionalismo, compromiso y vocación de servicio del personal docente y administrativo de Colegio Patria</li>
           <li class="circle-patria">Descuentos en pago de colegiaturas</li>
           <li class="circle-patria">Descuentos en colegiaturas para el caso de que tenga a más de un hijo inscrito en el Colegio</li>
           <li class="circle-patria">Existen otros muchos beneficios que dependen del empeño, el esfuerzo y los intereses de los propios usuarios.<br><br>
           Por ejemplo, usted puede ser beneficiario de una beca, pero acceder a ella implica un buen desempeño académico del alumno.
          También puede encontrar un foro y soporte para que su hijo(a) se desarrolle artística o deportivamente, lo cual está en función
           de sus destrezas y habilidades, así como de su constancia. Le sugerimos que acuda a nuestras instalaciones y pregunte qué otros beneficios pueden ser aplicables a su caso particular.</li>
         </ul>
        </div>
      </div>
    </div>
  </div>


<?php include("layouts/footer.php"); ?>
