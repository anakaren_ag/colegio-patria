<?php include("layouts/master.php"); ?>
  <div id="header-principios">
    <div class="container-fluid principiosbg valign-wrapper">
      <div class="row center-align">
        <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> PRINCIPIOS Y VALORES </h1>
      </div>
    </div>
  </div>
  <div id="principios">
    <div class="container-fluid">
      <div class="row center-align">
        <div class="col l4 m4 s12">
          <img class="responsive-img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s" src="img/mision.jpg">
          <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Desarrollar armónicamente todas las facultades del ser humano. </h2>
          <p class="prin-cont wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Sustenta la acción educativa del Colegio, su visión integral. Favorece el desarrollo de todas las potencialidades y talentos de los alumnos, considerados individual y socialmente. Busca que, a partir del conocimiento de sí mismos, construyan un proyecto de vida productiva y satisfactoria.</p>
        </div>
        <div class="col l4 m4 s12">
          <img class="responsive-img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s"  src="img/vision.jpg">
          <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Fortalecer el amor a la patria y la conciencia nacional.  </h2>
          <p class="prin-cont wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Implica la identificación y conservación de los valores, costumbres y tradiciones regionales y nacionales, así como una actitud comprometida con el país, participando como ciudadano activo en la defensa de su soberanía y proponiendo alternativas justas, de mejora social, en aras del bien común.</p>
        </div>
        <div class="col l4 m4 s12">
          <img class="responsive-img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s"  src="img/politica-calidad.jpg">
          <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Fomentar la conciencia e integración a la sociedad internacional a través de la solidaridad y la cooperación. </h2>
          <p class="prin-cont wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Con base en el respeto a la diversidad y la interculturalidad de los pueblos, busca desarrollar el sentido de pertenencia al mundo y trabajar por el bien de la humanidad, con base en la independencia y la justicia social.</p>
        </div>
      </div>
    </div>
  </div>
  <div id="principios-bullets">
    <div class="container">
      <div class="row">
        <div class="col l6 m6 s12">
          <h2> VALORES INSTITUCIONALES</h2>
          <ul>
            <li>Administración de los recursos.</li>
            <li>Amor a la verdad, a la belleza y adhesión al bien.</li>
            <li>Amor a la vida y conciencia ecológica.</li>
            <li>Amor a la Patria.</li>
            <li>Autodisciplina.</li>
            <li>Civilidad.</li>
            <li>Conciencia e integración en la sociedad internacional.</li>
            <li>Convivencia familiar y Social.</li>
            <li>Igualdad.</li>
            <li>Justicia.</li>
            <li>Libertad y responsabilidad.</li>
            <li>Paz.</li>
            <li>Paz individual y social.</li>
            <li>Respeto a sí mismo y a los demás.</li>
            <li>Solidaridad.</li>
          </ul>
        </div>
        <div class="col l6 m6 s12">
          <img class="responsive-img" src="img/principios-2.jpg">
        </div>
      </div>
    </div>
  </div>



<?php include("layouts/footer.php"); ?>
