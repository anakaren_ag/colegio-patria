<?php include("layouts/master.php"); ?>
<div id="header-maternal">
  <div class="container-fluid maternalbg valign-wrapper">
    <div class="row center-align">
      <h1> Galería de imágenes </h1>
    </div>
  </div>
</div>
<div class="container" id="form-gallery">
  <div class="row">
    <div class="col l6 s12 m6">
    <h4 class="title"> BUSCAR GENERAL </h4>
    <form action="#">
      <p>
      <input name="group1" type="radio" id="test1" />
      <label for="test1">Sí</label>
      <input name="group1" type="radio" id="test2" />
      <label for="test2">No</label>
      <div class="search-wrapper card search">
        <i class="material-icons">search</i>
     </div>
    </p>
    </div>
    <div class="col s12 m6 l2">

        <label>Año</label>
        <select class="browser-default">
          <option value="1">2000</option>
          <option value="2">2001</option>
          <option value="3">2002</option>
          <option value="1">2003</option>
          <option value="2">2004</option>
          <option value="3">2005</option>
          <option value="1">2006</option>
          <option value="2">2007</option>
          <option value="3">2008</option>
          <option value="1">2009</option>
          <option value="2">2010</option>
          <option value="3">2011</option>
          <option value="1">2012</option>
          <option value="2">2013</option>
          <option value="3">2014</option>
          <option value="1">2015</option>
          <option value="2">2016</option>
          <option value="3">2017</option>
        </select>
    </div>
    <div class="col s12 m6 l2">
        <label>Nivel</label>
        <select class="browser-default">
          <option value="1">Maternal</option>
          <option value="2">Preecolar</option>
          <option value="3">Primaria</option>
          <option value="2">Secundaria</option>
          <option value="3">Preparatoria</option>
        </select>
    </div>
    <div class="col s12 m6 l2">
        <label>Plantel</label>
        <select class="browser-default">
          <option value="1">Ángel</option>
          <option value="2">Iturbide</option>
        </select>
    </div>
   <div class="col s12 m6 l2">
      <div class="input-field col s12">
        <input id="evento" type="text">
        <label for="Evento">Evento</label>
      </div>
    </div>
    <div class="col s12 m6 l2">
      <div class="input-field col s12">
        <input id="descripcion" type="text">
        <label for="Descripción">Descripción</label>
      </div>
    </div>
    <div class="col s12 m6 l2">
      <div class="input-field col s12">
        <input id="titulo" type="text">
        <label for="titulo">Título</label>
      </div>
    </div>
  </div>
 </div>
</div>
</div>
<div class="container" id="galeria">
  <div class="row center">
    <div class="col l4 m6 s12 photo">
      <img class="materialboxed" src="img/primaria-01.jpg">
      <h5>Titulo de foto</h5>
    </div>
    <div class="col l4 m6 s12 photo">
      <img class="materialboxed" src="img/primaria-01.jpg">
      <h5>Titulo de foto</h5>
    </div>
    <div class="col l4 m6 s12 photo">
      <img class="materialboxed" src="img/primaria-01.jpg">
      <h5>Titulo de foto</h5>
    </div>
    <div class="col l4 m6 s12 photo">
      <img class="materialboxed" src="img/primaria-01.jpg">
      <h5>Titulo de foto</h5>
    </div>
    <div class="col l4 m6 s12 photo">
      <img class="materialboxed" src="img/primaria-01.jpg">
      <h5>Titulo de foto</h5>
    </div>
    <div class="col l4 m6 s12 photo">
      <img class="materialboxed" src="img/primaria-01.jpg">
      <h5>Titulo de foto</h5>
    </div>
  </div>
</div>
<?php include("layouts/footer.php"); ?>
