<?php include("layouts/master.php"); ?>
  <div id="header-planteles">
    <div class="container-fluid plantelesbg valign-wrapper">
      <div class="row center-align">
        <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> PLANTELES </h1>
      </div>
    </div>
  </div>
  <div id="plantel-angel">
    <div class="container-fluid">
      <div class="row">
        <div class="col l6 m6 s12">
          <img class="responsive-img wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay="1s" src="img/plantel-angel.jpg">
        </div>
        <div class="col l6 m6 s12 wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="1s">
          <h2> Plantel Ángel de la Independencia </h2>
          <p class= "planteles-content wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"> El Plantel Ángel de la Independencia es el de más antiguo de Colegio Patria;
          data del año 1994. Sus instalaciones albergan a los Niveles Maternal,
          Preescolar y Primaria. Construido con los mejores materiales, es una serie de
          modernas edificaciones diseñadas para brindar servicios educativos, con toda
          seguridad, a un gran número de alumnos. Sus interiores están perfectamente
          señalizados y son objeto de mantenimiento y modernización constantes. Un
          equipo capacitado se encarga de mantenerlas en perfecta limpieza. El Plantel
          Ángel de la Independencia, toma su nombre de la avenida en que se
          encuentra ubicado; ahí se encuentran las instalaciones siguientes:</p>
          <p class= "planteles-content">
            <b>Nivel Maternal:</b> Avenida Ángel de la Independencia N° 17, Colonia
            Metropolitana, 2a. Sección, Nezahualcóyotl, Estado de México. Teléfonos
            5793-6321 y 5797-9206.<br>
            <b>Nivel Preescolar:</b> Avenida Ángel de la Independencia N° 28, Colonia
            Metropolitana, 2a. Sección, Nezahualcóyotl, Estado de México. Teléfonos
            5793-6321 y 5797-9206.<br>
            <b>Nivel Primaria:</b> Avenida Ángel de la Independencia N° 16 y 18, Colonia
            Metropolitana, 2a. Sección, Nezahualcóyotl, Estado de México. Teléfonos
            5793-6321 y 5797-9206.
            </p>
        </div>
      </div>
    </div>
  </div>
  <div id="plantel-iturbide">
    <div class="container-fluid">
      <div class="row">
        <div class="col l6 m6 s12 wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="1s">
          <h2> Plantel  Iturbide </h2>
          <p class= "planteles-content wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"> Inaugurado en 1999, es la más reciente de nuestras instalaciones. Se trata de
          un conjunto de edificios interconectados, perfectamente delimitados, que fueron
          proyectados para ofrecer servicios educativos. En el Plantel Iturbide se
          encuentran los Niveles Primaria, Secundaria y Preparatoria –que tiene su
          ingreso en la avenida Nezahualcóyotl, pero se mantiene físicamente unida a

          todo el conjunto. Está construido, también, con los mejores y más resistentes
          materiales para dar la mayor seguridad posible a la gran cantidad de alumnos,
          padres de familia y personal docente y administrativo que diariamente acude al
          Colegio. Los interiores del Plantel Iturbide, además de ser modernos, están
          perfectamente señalizados y son motivo de mantenimiento y actualización
          constantes. Un conjunto de expertos, capacitados permanentemente, se
          encarga de vigilar las instalaciones y de mantenerlas en perfecto estado
          operativo y de limpieza. El Plantel Iturbide también toma su nombre de la calle
          en la que se ubicada; ahí se encuentran usted las instalaciones siguientes:</p>
          <p class= "planteles-content">
            <b>Nivel Primaria:</b> Iturbide N° 11, Colonia Raúl Romero, Nezahualcóyotl, Estado
            de México. Teléfonos 5765-5169 y 5765-4821.<br>
            <b>Nivel Secundaria:</b> Iturbide N° 11, Colonia Raúl Romero, Nezahualcóyotl,
            Estado de México. Teléfonos 5765-5169 y 5765-4821.<br>
            <b>Nivel Preparatoria:</b> Av. Nezahualcóyotl N° 6, Colonia Atlacomulco,
            Nezahualcóyotl, Estado de México. Teléfonos 5765-5169 y 5765-4821.
            </p>
        </div>
        <div class="col l6 m6 s12">
          <img class="responsive-img wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay="1s" src="img/plantel-iturbide.jpg">
        </div>
      </div>
    </div>
  </div>


<?php include("layouts/footer.php"); ?>
