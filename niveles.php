<?php include("layouts/master.php"); ?>
  <div id="header-niveles">
    <div class="container-fluid nivelesbg valign-wrapper">
      <div class="row center-align">
        <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> NIVELES </h1>
      </div>
    </div>
  </div>
  <div id="welcome-niveles" >
    <div class="container">
      <div class="row">
        <div class="col l6 m6 s12">
          <h2 class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.8s"> BIENVENIDOS A<br> COLEGIO PATRIA </h2>
        </div>
        <div class="col l6 m6 s12">
          <p class="niveles-content wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.8s">
            <b>Sociedad Cultural Colegio Patria, S. C.,</b> les da la más cordial bienvenida a este espacio virtual, en el que podrán conocer, entre otros aspectos, nuestro Modelo Educativo, los Planteles, las instalaciones, las características de cada uno de los Niveles Educativos que ponemos a su disposición –de Maternal a Preparatoria-, los programas y actividades que llevamos a cabo, los Talleres Extracurriculares que ofrecemos y los servicios que ponemos a su alcance para que su hijo(a) reciba una educación y una formación de calidad e innovadora. Lo invitamos también para que conozca nuestra filosofía educativa y los principios y valores que orientan y sustentan la labor pedagógica que realizamos.
          </p>
        </div>
      </div>
    </div>
  </div>
  <div id="niveles1" >
    <div class="container-fluid p-top-bottom">
      <div class="row">
        <div class="col l6 m6 s12 maternal-bg">
        </div>
        <div class="col l6 m6 s12">
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s"> MATERNAL </h3>
          <div class="line wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"></div>
          <p class="niveles-content wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> En 1997, después de tres años de actividad con demanda creciente, se estableció el Nivel Maternal para responder a las inquietudes de las familias que planteaban la necesidad de encontrar opciones para que sus hijos recién nacidos recibieran atención entre el primer mes de vida y los tres años de edad, e iniciaran su formación en una institución seria y profesional que les garantizara una misma filosofía educativa.<br><br>
            El propósito fundamental del Plan de Estudios del Nivel Maternal es lograr el desarrollo físico e intelectual de los niños, garantizar su seguridad, integridad y respeto, y favorecer su crecimiento en un ambiente sano y feliz. El Nivel cuenta con la infraestructura siguiente: Ludoteca/Estimulación temprana, Comedor, Enfermería, Invernadero, Salón de cómputo y Salón de educación artística.
          </p>
          <a href="maternal.php">
            <div class="btn-patria wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"> CONOCE MÁS </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div id="niveles2" >
    <div class="container-fluid p-bottom">
      <div class="row">
        <div class="col l6 m6 s12">
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s"> PREESCOLAR </h3>
          <div class="line wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"></div>
          <p class="niveles-content wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> El Nivel Preescolar considera la necesidad y el derecho de los niños a jugar y, simultáneamente, a prepararse para desarrollarse en una sociedad que tendrá mayores exigencias. Bajo estas premisas, el Programa de Estudios del Nivel Preescolar plantea el desarrollo de competencias a través de la enseñanza de conocimientos y de la adquisición de habilidades y actitudes positivas en los niños.<br> <br>
            Para conseguirlo, el Nivel Preescolar cuenta con la infraestructura siguiente: Ludoteca, Enfermería, Laboratorio de cómputo, Salón de Educación Artística, Áreas deportivas, Área de juegos.
          </p>
          <a href="maternal.php">
            <div class="btn-patria wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> CONOCE MÁS </div>
          </a>
        </div>
        <div class="col l6 m6 s12 maternal-bg">
        </div>
      </div>
    </div>
  </div>
  <div id="niveles3" >
    <div class="container-fluid p-top-bottom">
      <div class="row">
        <div class="col l6 m6 s12 maternal-bg">
        </div>
        <div class="col l6 m6 s12">
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s"> PRIMARIA </h3>
          <div class="line wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"></div>
          <p class="niveles-content wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> El Programa de Estudios del Nivel Primaria se diseñó para desarrollar en el alumno las habilidades intelectuales que le permitirán comprender, procesar, analizar y aplicar competencias matemáticas, científicas y comunicativas, y para fortalecer la autonomía y la toma de decisiones personales. En este proceso, el alumno fortalece su disciplina y refuerza hábitos de conducta que, sustentados en los valores universales, lo harán triunfador y exitoso.<br><br>
            El egresado de Primaria es poseedor de las competencias comunicativas, científicas, Tecnológicas, artísticas, éticas y físicas, así como de los hábitos de conducta y disciplina para avanzar hacia la autonomía y la toma de decisiones personales, fincadas en la práctica de los valores universales.
          </p>
          <a href="primaria.php">
            <div class="btn-patria wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> CONOCE MÁS </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div id="niveles4" >
    <div class="container-fluid p-bottom">
      <div class="row">
        <div class="col l6 m6 s12">
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s"> SECUNDARIA </h3>
          <div class="line wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"></div>
          <p class="niveles-content wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> El Programa de Estudios del Nivel Secundaria está enfocado en formar jóvenes tolerantes, responsables, creativos y preparados para afrontar y superar retos con éxito. Se fundamenta en los principios básicos siguientes: Aprender a conocer, aprender a hacer, aprender a convivir y aprender a ser. La infraestructura disponible para que la Secundaria cumpla con sus objetivos es la siguiente: Biblioteca, Cafetería, Enfermería, Laboratorio de ciencias, Laboratorio de cómputo, Laboratorio de Fotografía, Salón de Educación Artística, Salón de Dibujo, Salón de Artes Plásticas, Salón de Usos Múltiples con equipo audiovisual, Espacios deportivos y recreativos.
          </p>
          <a class="secundaria.php">
            <div class="btn-patria wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> CONOCE MÁS </div>
          </a>
        </div>
        <div class="col l6 m6 s12 maternal-bg">
        </div>
      </div>
    </div>
  </div>
  <div id="niveles5" >
    <div class="container-fluid p-top-bottom">
      <div class="row">
        <div class="col l6 m6 s12 maternal-bg">
        </div>
        <div class="col l6 m6 s12">
          <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s"> PREPARATORIA </h3>
          <div class="line wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.8s"></div>
          <p class="niveles-content wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> El Programa de Estudios del Nivel Preparatoria está diseñado para que los jóvenes egresados, además de satisfacer los requisitos de la UNAM para emprender estudios universitarios, sean personas tolerantes, responsables, creativas y dispuestas a superar con éxito los obstáculos que se les presenten. Siguiendo la línea establecida desde el Nivel académico previo, se fundamenta en los principios siguientes: Aprender a conocer, aprender a hacer, aprender a convivir y aprender a ser. Para ello, cuenta con la infraestructura siguiente: Laboratorio de ciencias, Laboratorio de cómputo, Laboratorio de Física y Química, Biblioteca, Salón de Educación Artística, Salón de dibujo y artes plásticas, Salón de Usos Múltiples con equipo audiovisual, Enfermería, Cafetería, Espacios deportivos y recreativos.
          </p>
          <a class="preparatoria.php">
            <div class="btn-patria wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.8s"> CONOCE MÁS </div>
          </a>
        </div>
      </div>
    </div>
  </div>

<?php include("layouts/footer.php"); ?>
