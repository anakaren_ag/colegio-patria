<?php include("layouts/master.php"); ?>
<div class="container-fluid">
  <div class="slider" id="slider-patria">
    <ul class="slides">
      <li>
        <img src="img/carousel1.jpg"> <!-- random image -->
        <div class="caption center-align">
          <h1 class="slide-title">El mejor REGALO para tus hijos.</h1>
          <h5 class="light grey-text text-lighten-3 slide-subtitle">Otorgales a tus hijos la mejor educación que puedan recibir, un regalo para toda su vida.</h5>
          <a class="btn btn-patria">ENTÉRATE</a>
        </div>
      </li>
      <li>
        <img src="img/carousel2.jpg"> <!-- random image -->
        <div class="caption center-align">
          <h2 class="slide-title">Trae a tus hijos</h1>
            <h5 class="light grey-text text-lighten-3 slide-subtitle">Ven con tu familia a conocer una de las mejores instituciones educativas del pais.</h5>
            <a class="btn btn-patria">ENTÉRATE</a>
          </div>
        </li>
        <li>
          <img src="img/carousel3.jpg"> <!-- random image -->
          <div class="caption center-align">
            <h2 class="slide-title">Inscripciones abiertas</h1>
              <h5 class="light grey-text text-lighten-3 slide-subtitle">Comenzamos nuestro periodo de inscripciones.</h5>
              <a class="btn btn-patria">ENTÉRATE</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <section id="about">
      <div class="container">
        <div class="title">
          <h1 class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0.8s">Nuestra Institución</h1>
          <div class="containtext wow fadeIn" data-wow-duration="2s" data-wow-delay="0.9s">
            <span>Sociedad Cultural Colegio Patria, S. C.</span> considera a los alumnos que asisten a sus instalaciones
            como personas poseedoras de una naturaleza fí­­sica y racional.<br></br>

            <span>Colegio Patria</span> fue fundado el 15 de julio de 1994 con la finalidad de contribuir a la formación del
            ser humano, a través de una educación de calidad, promoviendo entre sus participantes
            el compromiso de brindar a la sociedad los mejores esfuerzos para contribuir al engrandecimiento del país.
          </div>
        </div>
      </div>
    </section>
    <section id="icos">
      <div class="container">
        <div class="row icontainer">
          <div class="col l4 m4 s12">
            <img class="icos wow zoomIn" data-wow-duration="2s" data-wow-delay="1s" src="/img/university.svg" alt="">
            <span class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">Reconocida como una institución emprendedora.</span>
          </div>
          <div class="col l4 m4 s12 centa">
            <img class="icos wow zoomIn" data-wow-duration="2s" data-wow-delay="1s" src="/img/mortarboard.svg" alt="">
            <span class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">Innovamos calidad educativa.</span>
          </div>
          <div class="col l4 m4 s12">
            <img class="icos wow zoomIn" data-wow-duration="2s" data-wow-delay="1s" src="/img/quality.svg" alt="">
            <span class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1s">Contamos con certificación ISO 9000.</span>
          </div>
        </div>
      </div>
    </section>
    <section id="beneficios">
      <div class="fluid-container graybg">
        <div class="row">
          <div class="col l6 m6 s12 bg-childs">
          </div>
          <div class="col l6 m6 s12">
            <div class="title">
              <h3 class="wow fadeIn" data-wow-duration="2s" data-wow-delay="1.2s">Beneficios</h3>
            </div>
            <div class="text-container wow slideInRight" data-wow-duration="2s" data-wow-delay="1.2s">
              <p>Son múltiples los beneficios que Colegio Patria ofrece a los usuarios de sus servicios educativos.</p>
              <div class="list-col">
                <ul>
                  <li>- Seguridad</li>
                  <li>- Transporte Escolar</li>
                  <li>- Actividades deportivas</li>
                  <li>- Excursiones</li>
                  <li>- Cafeterias</li>
                </ul>
              </div>

              <div class="list-col">
                <ul>
                  <li>- Amplias Áreas</li>
                  <li>- Laboratorios de cómputo</li>
                  <li>- Actividades artisticas</li>
                  <li>- Biblioteca</li>
                  <li>- Valores</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="servicios">
      <div class="container-fluid">
        <div class="row">
          <div class="col l4 m12 s12">
            <img class="service-img wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"  src="/img/Image-3.jpg">
            <h4 class="servicios-title wow fadeIn" data-wow-duration="2s" data-wow-delay="1.7s"  > TALLERES EXTRACURRICULARES </h4>
            <div class="line"></div>
            <div class="text-container wow fadeIn" data-wow-duration="2s" data-wow-delay="1.7s"  > Lo necesario para la mejora continua </div>
          </div>
          <div class="col l4 m12 s12">
            <img class="service-img wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"   src="/img/image-4.jpg">
            <h4 class="servicios-title wow fadeIn" data-wow-duration="2s" data-wow-delay="1.7s"> EDUCACIÓN COMPLEMENTARIA </h4>
            <div class="line"></div>
            <div class="text-container wow fadeIn" data-wow-duration="2s" data-wow-delay="1.7s"> Lo necesario para la mejora continua </div>
          </div>
          <div class="col l4 m12 s12">
            <img class="service-img wow zoomIn" data-wow-duration="2s" data-wow-delay="1.5s"   src="/img/image-5.jpg">
            <h4 class="servicios-title wow fadeIn" data-wow-duration="2s" data-wow-delay="1.7s"> ESCUELA <br> PARA PADRES </h4>
            <div class="line"></div>
            <div class="text-container wow fadeIn" data-wow-duration="2s" data-wow-delay="1.7s"> Lo necesario para la mejora continua </div>
          </div>
        </div>
      </div>
    </section>
    <section id="carousel2">
      <div class="container-fluid">
        <div class="row">
          <div class="col l6 s12 m12">
            <div class="slider">
              <ul class="slides">
                <li>
                  <img src="img/carousel-2.jpg"> <!-- random image -->
                </li>
                <li>
                  <img src="img/carousel3.jpg"> <!-- random image -->
                </li>
                <li>
                  <img src="img/carousel4.jpg"> <!-- random image -->
                </li>
                <li>
                  <img src="img/carousel5.jpg">  <!-- random image -->
                </li>
              </ul>
            </div>
          </div>
          <div class="col l6 s12 m12">
            <img class="icon-caru responsive-img" src="img/teacher-desk.png">
            <div class="text-caru wow slideInRight" data-wow-duration="2s" data-wow-delay="1.2s">
              <h4 class="title-caru"> Las mejores instalaciones </h4>
              <p> Contamos con instalaciones reconocidas internacionalmente. </p>
            </div>
            <br>
            <img class="icon-caru responsive-img" src="img/healthy-food.png">
            <div class="text-caru wow slideInRight" data-wow-duration="2s" data-wow-delay="1.2s">
              <h4 class="title-caru">Profesores certificados </h4>
              <p> Todos nuestros profesores cuentan con maestrias y doctorados. </p>
            </div>
            <br>
            <img class="icon-caru responsive-img" src="img/safe.png">
            <div class="text-caru wow slideInRight" data-wow-duration="2s" data-wow-delay="1.2s">
              <h4 class="title-caru"> Amplia seguridad </h4>
              <p> Nuestras instalaciones cuentan con seguridad y monitoreo 24hrs. </p>
            </div>
            <br>
            <img class="icon-caru responsive-img" src="img/activities.png">
            <div class="text-caru wow slideInRight" data-wow-duration="2s" data-wow-delay="1.2s">
              <h4 class="title-caru"> Varias actividades </h4>
              <p> Contamos con mas de 20 actividades externas al ciclo escolar. </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="pre-footer">
      <div class="container-fluid graybg">
        <div class="row valign-wrapper nomargin">
          <div class="padding"></div>
          <div class="col l4 s12 m4">
            <img class="icon-caru responsive-img" src="img/calendar.png">
            <br>
            <div class="text-caru">
              <h4 class="title-caru"> ¡Ven a conocernos! </h4>
              <p> Con gusto uno de nuestros asesores<br> te dara un recorrido por todas <br>nuestras instalaciones.</p>
              <div class="line"></div>
            </div>
          </div>
          <div class="col l4 s12 m4">
            <img class="icon-caru responsive-img" src="img/icon-kids.png">
            <br>
            <div class="text-caru">
              <h4 class="title-caru"> Trae a tus hijos </h4>
              <p> Ven con tu familia a conocer <br> una de las mejores instituciones<br> educativas del pais. </p>
              <div class="line"></div>
            </div>
          </div>
          <div class="col l4 s12 m4">
            <img class="icon-caru responsive-img" src="img/icon-books.png">
            <br>
            <div class="text-caru2">
              <h4 class="title-caru"> Inscripciones abiertas </h4>
              <p> Comenzamos nuestro periodo<br> de inscripciones. </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include("layouts/footer.php"); ?>
