<?php include("layouts/master.php"); ?>
  <div id="header-nosotros">
    <div class="container-fluid nosotrosbg valign-wrapper">
      <div class="row center-align">
        <h1 class="wow bounceInDown" data-wow-duration="1s" data-wow-delay="0.8s"> QUIENES SÓMOS </h1>
      </div>
    </div>
  </div>
  <div id="historia">
    <div class="container">
     <div class="row center-align">
      <div class="col l12 m12 s12">
        <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s"> NUESTRA HISTORIA </h2>
        <p class= "nos-content"> Sociedad Cultural Colegio Patria, S. C., nació en el año de 1994 para contribuir a la formación del ser humano, así como al engrandecimiento de México mediante una educación de calidad.</p>
      </div>
    </div>
  </div>
  <div id="nos-mi-vi">
    <div class="container-fluid">
      <div class="row center-align">
        <div class="col l4 m4 s12">
          <img class="responsive-img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s" src="img/mision.jpg">
          <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> MISIÓN </h2>
          <p class="nos-content wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Ofrecer a nuestros alumnos una educación innovadora de la más alta calidad científica, tecnológica y artística, sustentada en la autodisciplina, el respeto a los demás, el amor a la patria y la práctica de los valores universales, como base sólida para su desarrollo personal y profesional, de manera que influyan positivamente en el avance sociocultural de su entorno local, nacional e internacional.</p>
        </div>
        <div class="col l4 m4 s12">
          <img class="responsive-img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s"  src="img/vision.jpg">
          <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> VISIÓN </h2>
          <p class="nos-content wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Consolidar a Sociedad Cultural Colegio Patria, S. C., como una institución integrada por individuos profesionales, responsables, con un elevado sentido ético, perseverantes, y comprometidos individual y socialmente en la formación integral de personas que se caractericen por la calidad y excelencia de sus conocimientos académicos, por ser poseedores de un espíritu científico y por la solidez de sus principios, de manera que contribuyan al desarrollo científico, tecnológico, social, cultural y económico de nuestra sociedad</p>
        </div>
        <div class="col l4 m4 s12">
          <img class="responsive-img wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s"  src="img/politica-calidad.jpg">
          <h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> POLÍTICA DE CALIDAD </h2>
          <p class="nos-content wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s"> Contribuir a la formación integral de nuestros alumnos a través del desarrollo de las competencias necesarias para integrarse a su entorno social, cultural y productivo, mediante un modelo educativo de vanguardia, basado en el cumplimiento de la normatividad oficial, procesos educativos de calidad y personal comprometido, capacitado y actualizado, con el fin de lograr la eficacia y la mejora continua para satisfacer las necesidades de alumnos, padres de familia y sociedad en general.</p>
        </div>
      </div>
    </div>
  </div>


<?php include("layouts/footer.php"); ?>
